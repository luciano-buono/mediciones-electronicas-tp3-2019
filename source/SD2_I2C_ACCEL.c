/* Copyright 2018, DSI FCEIA UNR - Sistemas Digitales 2
 *    DSI: http://www.dsi.fceia.unr.edu.ar/
 * Copyright 2018, Gustavo Muro
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/

// Standard C Included Files
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include <math.h>
// Project Included Files
#include "SD2_board.h"
#include "mma8451.h"
#include "Clock.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

uint8_t RC=5;
uint8_t T_conv= 200;
uint8_t fclock=10*10^3;

int contador,delay,latch_antcoma,latch_despcoma;
float latch;

boolean GPIO_OUT1, GPIO_OUT2, GPIO_IN, mostrar_flag;
/*==================[internal functions declaration]=========================*/

void Inicializacion();
void SysTick_Init(void);
void MostrarPorPantalla();

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/


int main(){

    Inicializacion();

    for(;;){
        if(mostrar_flag){
            NVIC_DisableIRQ(SysTick_IRQn);
            latch=0.39*(contador+70) - 125 - 30.5 -1;
            contador=-1000;
            MostrarPorPantalla();
            mostrar_flag=0;
            board_clearGPIO1();
            board_clearGPIO2();
            delay=10;
            NVIC_EnableIRQ(SysTick_IRQn);
        }

        if(contador>=0)
            board_setGPIO1();
    }
}

void SysTick_Init(void){
	SysTick_Config(SystemCoreClock / 10000U);
}



void MostrarPorPantalla(){
	latch_antcoma= (int)latch;
	latch_despcoma=(latch - latch_antcoma)*10;
    //printf("\n%d,%d", latch/10 ,latch -((int)latch/10)*10);
	printf("\n%d,%d", latch_antcoma ,latch_despcoma);
}

void Inicializacion(){
    mostrar_flag= 0;
    contador=-1000;
    SysTick_Init();
    board_init();
    board_clearGPIO1();
    board_setGPIO2();
    delay = 100;
    NVIC_EnableIRQ(SysTick_IRQn);
    while (delay > 0){
    	printf("Inicializando: %d\n", delay);
    }
    board_clearGPIO2();
}

void SysTick_Handler(void){

	if (delay >0){
		delay --;
	}
	else{
		if(contador < 0)
			contador++;
		else if(board_getSw(BOARD_SW_ID_1) || !board_getGPIOIN() ){
			  contador++;
			  board_setLed(BOARD_LED_ID_ROJO,BOARD_LED_MSG_ON);
		}
		else if(contador >=0){
			mostrar_flag=1;
			board_setGPIO2();
			board_setLed(BOARD_LED_ID_ROJO,BOARD_LED_MSG_OFF);
		}
	}
}


/*==================[end of file]============================================*/


